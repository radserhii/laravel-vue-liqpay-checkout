<?php

namespace App\Services;


use App\SDK\LiqPay;

class LiqPayService
{
    private $public_key = 'sandbox_i68999667394';
    private $private_key = 'sandbox_vdofuckg0d81IuZ2cr2GASURyKB532PcsHYOICWA';
    /**
     * @var LiqPay
     */
    private $liqPayClient;

    /**
     * LiqPayService constructor.
     */
    public function __construct()
    {
        $this->liqPayClient = new LiqPay($this->public_key, $this->private_key);
    }

    public function getForm(int $amount): string
    {
        return $this->liqPayClient->cnb_form([
            'action' => 'pay',
            'amount' => $amount,
            'currency' => 'UAH',
            'description' => 'description text',
            'order_id' => uniqid('test_order'),
            'version' => '3',
            'language' => 'uk',
            'result_url' => config('liqpay.result_url'),
            'server_url' => config('liqpay.server_url')
        ]);
    }

    public function checkSignature(string $signature, string $data): bool
    {
        return $signature === base64_encode(sha1($this->private_key . $data . $this->private_key, 1));
    }

}
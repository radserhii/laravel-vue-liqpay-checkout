<?php

namespace App\Http\Controllers;

use App\Services\LiqPayService;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * @var LiqPayService
     */
    private $liqPayService;

    /**
     * Create a new controller instance.
     *
     * @param LiqPayService $liqPayService
     */
    public function __construct(LiqPayService $liqPayService)
    {
        $this->liqPayService = $liqPayService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function getPaymentForm(Request $request)
    {
        if (isset($request->amount) && $request->amount > 0) {
            return $this->liqPayService->getForm($request->amount);
        }
        return null;
    }

    public function paymentCallback(Request $request)
    {
        $signature = $request->signature;
        $data = $request->data;
        \Log::debug(print_r($this->liqPayService->checkSignature($signature, $data), true));
        if ($this->liqPayService->checkSignature($signature, $data)) {
            \Log::debug(print_r(base64_decode($data), true));
        }
    }
}

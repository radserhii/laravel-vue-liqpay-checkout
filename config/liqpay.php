<?php

return [
    'result_url' => env('LIQPAY_RESULT_URL'),
    'server_url' => env('LIQPAY_SERVER_URL'),
];